class AddStatusIdToLeads < ActiveRecord::Migration[5.2]
  def change
    add_reference :leads, :status, foreign_key: true
  end
end
