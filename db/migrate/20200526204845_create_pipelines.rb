class CreatePipelines < ActiveRecord::Migration[5.2]
  def change
    create_table :pipelines do |t|
      t.string :uid
      t.string :title

      t.timestamps
    end
  end
end
