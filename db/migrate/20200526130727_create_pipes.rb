class CreatePipes < ActiveRecord::Migration[5.2]
  def change
    create_table :pipes do |t|
      t.string :name
      t.string :uid

      t.timestamps
    end
  end
end
