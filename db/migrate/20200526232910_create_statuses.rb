class CreateStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :statuses do |t|
      t.string :uid
      t.string :name
      t.references :pipeline, foreign_key: true

      t.timestamps
    end
  end
end
