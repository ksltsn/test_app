class CreateLeads < ActiveRecord::Migration[5.2]
  def change
    create_table :leads do |t|
      t.string :uid
      t.string :email
      t.string :price
      t.references :pipeline, foreign_key: true

      t.timestamps
    end
  end
end
