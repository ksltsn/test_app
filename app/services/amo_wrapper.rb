class AmoWrapper
  class << self

  require 'net/https'
  require 'json'

  Domain = Rails.application.credentials.dig(:amocrm, :domain)
  Token = Rails.application.credentials.dig(:amocrm, :token)
  Headers = {
    'Authorization' => "Bearer #{Token}",
    'Content-Type'  => 'application/json',
    'Accept'        => 'application/json'
  }

  # Получение сделок
  def leads
    uri = URI("#{Domain}/api/v2/leads")

    response = get_request(uri)
    JSON.parse(response.body)['_embedded']['items'] if response.body
  end

  # Получение воронок
  def pipelines
    uri = URI("#{Domain}/api/v2/pipelines")

    pipes = []
    response = get_request(uri)

    JSON.parse(response.body)['_embedded']['items'].each do |pipeline|
      name = pipeline[1]['name']
      uid  = pipeline[1]['id']
      pipes << [uid, name]
    end
    pipes
  end

  # Добавление сделок в этап "зарегистрировано"
  def add_lead(data)
    uri = URI("#{Domain}/api/v2/leads")

    pipeline = Pipeline.find_by(id: data['pipeline_id'])
    status = Status.where(pipeline: pipeline.id, name: 'Зарегистрировано').first

    params = {
      add: [{
        name: data['email'].to_s,
        status_id: status.uid.to_i}]
    }

    response = post_request(params, uri)
    JSON.parse(response.body)
  end

  # Обновление этапа сделки с "зарегистрировано" на "оплачено"
  def update_lead(data)
    uri = URI("#{Domain}/api/v2/leads")

    lead = Lead.find(data['id'])
    pipeline = lead.pipeline
    status = pipeline.statuses.where(pipeline_id: pipeline.id, name: 'Оплачено').last

    params = {
      update: [{
         id: lead.uid,
         updated_at: "#{(DateTime.rfc3339(DateTime.now.to_s).to_time.to_i)}",
         status_id: status['uid'],
         sale: data['price']}]
    }

    response = post_request(params, uri)
    JSON.parse(response.body)
  end

  # Создание новой воронки
  def add(pipe_data)
    uri    = URI("#{Domain}/private/api/v2/json/pipelines/set")
    params = {
      "request": {
        "pipelines": {
          "add": [{
            "name": pipe_data['title'].to_s,
            "statuses": {
              "0": {
                "color": "#fffeb2",
                "name": "Зарегистрировано",
                "sort": 0
              },
              "1": {
                "color": "#fffeb2",
                "name": "Оплачено",
                "sort": 10
              }
            },
            "is_main": "false"
          }]
        }
      }
    }

    response = post_request(params, uri)
    JSON.parse(response.body)['response']['pipelines']['add']['pipelines']
  end

  # Удаление конкретной воронки
  def delete(pipe_uid)
    params       = { "request": { "id": pipe_uid } }
    uri          = URI("#{Domain}/private/api/v2/json/pipelines/delete")
    response = post_request(params, uri)
  end

  private

  def post_request(params, uri)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.post(uri.path, params.to_json, Headers)
  end

  def get_request(uri)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.get(uri.path, Headers)
  end
  end
end
