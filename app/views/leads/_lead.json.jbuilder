json.extract! lead, :id, :uid, :email, :price, :pipeline_id, :created_at, :updated_at
json.url lead_url(lead, format: :json)
