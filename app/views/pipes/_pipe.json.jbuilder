json.extract! pipe, :id, :name, :uid, :created_at, :updated_at
json.url pipe_url(pipe, format: :json)
