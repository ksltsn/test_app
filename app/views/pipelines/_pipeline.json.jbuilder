json.extract! pipeline, :id, :uid, :title, :created_at, :updated_at
json.url pipeline_url(pipeline, format: :json)
