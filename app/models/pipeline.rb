class Pipeline < ApplicationRecord
  has_many :leads, dependent: :destroy
  has_many :statuses, dependent: :destroy
end
