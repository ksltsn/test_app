class LeadsController < ApplicationController
  before_action :set_lead, only: [:update]
  before_action :authenticate_user!

  def new
    @lead = Lead.new
  end

  def create
    lead = AmoWrapper.add_lead(lead_params)
    if lead
      pipeline = Pipeline.find_by(id: lead_params['pipeline_id'])
      status   = Status.where(pipeline: pipeline.id, name: 'Зарегистрировано').first

      @lead           = Lead.new
      @lead.pipeline  = pipeline
      @lead.status_id = status.id
      @lead.email     = lead_params['email']
      @lead.uid       = lead['_embedded']['items'][0]['id']

      respond_to do |format|
        if @lead.save
          format.html { redirect_to pipeline, notice: 'Lead was successfully created.' }
        else
          format.html { render :new }
        end
      end

    end
  end

  def update
    AmoWrapper.update_lead(params)
    @pipeline = @lead.pipeline
    @lead.price = params['price']
    @lead.status_id = @lead.pipeline.statuses.where(pipeline_id: @pipeline.id, name: 'Оплачено').last.id

    respond_to do |format|
      if @lead.save
        format.html { redirect_to @pipeline, notice: 'Lead was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_lead
    @lead = Lead.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def lead_params
    params.require(:lead).permit(:uid, :email, :price, :pipeline_id)
  end
end
