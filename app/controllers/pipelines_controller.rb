class PipelinesController < ApplicationController
  before_action :set_pipeline, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @pipelines = Pipeline.all
  end

  def show
    @leads = AmoWrapper.leads
    if @leads
      @leads = @leads.select { |lead| lead["pipeline_id"].to_s == @pipeline.uid }
      @leads.each do |lead|

        Lead.find_or_create_by(
          uid: lead['id'],
          email: lead['name'],
          pipeline_id: lead['pipeline_id'],
          status_id: lead['status_id']
        )
      end
      @leads = @pipeline.leads
    end
  end

  def new
    @pipeline = Pipeline.new
  end

  def create
    pipeline = AmoWrapper.add(pipeline_params).first[1]
    if pipeline
      @pipeline       = Pipeline.new
      @pipeline.uid   = pipeline['id']
      @pipeline.title = pipeline['name']

      pipeline['statuses'].each do |st|
        status          = Status.new
        status.uid      = st[1]['id']
        status.pipeline = @pipeline
        status.name     = st[1]['name']
        status.save!
      end


      respond_to do |format|
        if @pipeline.save
          format.html { redirect_to @pipeline, notice: 'Pipeline was successfully created.' }
          format.json { render :show, status: :created, location: @pipeline }
        else
          format.html { render :new }
          format.json { render json: @pipeline.errors, status: :unprocessable_entity }
        end
      end

    end
  end

  def update
    respond_to do |format|
      if @pipeline.update(pipeline_params)
        format.html { redirect_to @pipeline, notice: 'Pipeline was successfully updated.' }
        format.json { render :show, status: :ok, location: @pipeline }
      else
        format.html { render :edit }
        format.json { render json: @pipeline.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @pipeline.destroy
    respond_to do |format|
      format.html { redirect_to pipelines_url, notice: 'Pipeline was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_pipeline
    @pipeline = Pipeline.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def pipeline_params
    params.require(:pipeline).permit(:uid, :title)
  end
end
