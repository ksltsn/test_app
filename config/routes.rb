Rails.application.routes.draw do
  root to: 'pipelines#index'

  devise_for :users, controllers: {omniauth_callbacks: 'omniauth'}

  resources :statuses
  resources :pipelines do
    resources :leads
  end

end
